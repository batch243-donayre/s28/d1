// CRUD
/*
	-CRUD operations are the heart of any backend application.
	-mastering the CRUD operation is essential for any developer.
	-This helps in building character and increasing exposure to logical statements that will help us manipulate.
	-mastering the CRUD operation of any languages makes us a valuable developer and makes the work easier for us to deal with hure amounts of information.
*/

//[section] Inserting document (Create)


	/*Insert one document*/
	/*
		- since MongoDB delas with objects as it's structure fro documents, we can easily create them by providing objects into our methods.
		-mongo shel also uses JavaScript for it's syntax which makes it convenient for us to understand it's code.

		syntax:
			db.collectionName.insertOnce({object});
		Javascript:
			object.object.method({object});
	*/

	db.users.insert({
		firstName: "Jane",
		lastname: "Doe",
		age: 21,
		contact: {
			phone: "12345678",
			email: "janedoe@gmail.com"
		},
		courses: ["CSS", "JavaScript", "Python"],
		department: "none"
	})

// Insert Many
/*
	-Syntax:
	db.collectionName.insertMany({ObjectA}, {ObjectB})
*/

	db.users.insertMany([{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact:{
			phone: "87654321",
			email: "stephenhawking@gmail.com"
		},
		courses:["Python", "React", "PHP"],
		department: "none"
	}, {
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact:{
			phone: "87654321",
			email: "neilarmstrong@gmail.com"
		},
		courses:["React", "Laravel", "Sass"],
		department: "none"
	}])


//[section] FINDING DOCUMENTS (READ)
/*
	-If multiple documents match the criteria for finding s document only the first document that matches the search term will be returned.
	- this is based from the order that the documents are stored in a collection

	Syntax:
		db.collectionName.find()
		db.collectionName.find({field:value});
*/
	// leaving the search criteria empty will retrieve all the documents

	db.users.find();

	db.users.find({firstName: "Stephen"});

	// pretty method
	// pretty method allows us to be able to view the documents returned by our terminals in a better format.

	db.users.find({firstName: "Stephen"}).pretty();

	// finding documents with multiple parameters
	/*
		-Syntax:
			db.collectionName.find({fieldA: valueA, fieldB: valueB})
	*/

	db.users.find({lastName: "Armstrong", age: 82}).pretty();


// [Section] Updating Documents
	// Updating a single document

	db.users.insert({
		firstName: "Test",
		lastName:"Test",
		age: 0,
		contact: {
			phone: "00000000",
			email: "test@gmail.com"
		},
		course: [],
		department: "none"
	})

/*
	-just like the find method, updateOne method will only manipulate a single document. First document that matches the search criteria will be updated.

	Syntax:
		db.collectionName.updateOne({criteria}, {$set: {field: value}})
*/

	db.users.updateOne(
		{firstName: "Bill"},
		{
			$set: {
				lastName: "Gates",
				age: 65,
				contact: {
					phone: "12345678",
					email: "bill@gmail.com"
				},
				courses: ["PHP", "Laravel", "HTML"],
				department: "Operations",
				status: "active"
			}
		}
	)

// Updating multiple documents
/*
	db.collectionName.updateMany({criteris}, {$set: {field: value}})
*/

	db.users.updateMany({department: "none"},{
		$set: {
			department: "HR"
		}
	})

	// Replace One
	// can be used if replacing the whole document necessary
	/*
		Syntax:
		db.collection.replaceOne({criteria}, {object})
	*/

	db.users.replaceOne({firstName: "Bill"},
		{
			firstName: "Chris",
			lastName: "Mortel",
			age: 14,
			contact:{
				phone: "12345678",
				email: "chris@gmail.com"
			},

			courses: ["PHP", "Laravel", "HTML"],
				department: "Operations"
		}

)

//[section] Deleting documents(delete)

	db.users.insert({
		firstName:"Test"
	})

//Delete a single document
	/*

	*/

	db.users.deleteOne({firstName: "test"});


//delete Many
	/*
		-be care when using the deleteMany method.
	*/

	db.users.deleteMany({firstName: "Chris"});


	//[section] Advanced queries
	//Query an embedded document(type object - considered as documents)

	db.users.find({
		contact: {
			phone: "87654321",
			email: "stephenhawking@gmail.com"
		}
	})


// query on nested field
	db.users.find({"contact.phone": "87654321"});

//querying an array with exact elements
	db.users.find({courses: ["Python", "React", "PHP"]});

//querying an array without regards to order and elements
	db.users.find({courses: {$all: ["React", "Laravel"]} });